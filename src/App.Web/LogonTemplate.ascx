﻿<%@ Control Language="C#" CodeBehind="LogonTemplate.ascx.cs" ClassName="LogonTemplate" Inherits="WebApp.LogonTemplate" %>
<%@ Register Assembly="DevExpress.ExpressApp.Web.v21.1" Namespace="DevExpress.ExpressApp.Web.Templates.ActionContainers"
TagPrefix="xaf" %>
<%@ Register Assembly="DevExpress.ExpressApp.Web.v21.1" Namespace="DevExpress.ExpressApp.Web.Templates.Controls"
TagPrefix="xaf" %>
<%@ Register Assembly="DevExpress.ExpressApp.Web.v21.1" Namespace="DevExpress.ExpressApp.Web.Controls"
TagPrefix="xaf" %>
<%@ Register Assembly="DevExpress.ExpressApp.Web.v21.1" Namespace="DevExpress.ExpressApp.Web.Templates"
TagPrefix="xaf" %>

<% %>
<meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0">
<style type="text/css">
    .right { float: right; }

    .CardGroupContent { padding: 0px !important; }

    .CardGroupBase { border: none !important; }

    .menuButtons .dxm-item {
        background-color: #2c86d3 !important;
        border: none !important;
        padding-left: 3px !important;
        padding-right: 3px !important;
    }

    .menuButtons .dxm-item .dx-vam { color: white; }

    .menuButtons .dxm-item.dxm-hovered, .menuButtons .dxm-item.dxm-hovered a.dx {
        background-color: #2c86d3 !important;
        border: none !important;
    }

    .menuButtons .dxm-noImages .dxm-item .dxm-content, .menuButtons .dxm-item.dxm-noImage .dxm-content {
        -webkit-box-shadow: none;
        border: none;
        padding: 14px 38px 14px 38px !important;
    }

    .LogonItemClassCSS { padding-bottom: 15px; }

    .LogonTextClassCSS {
        padding-bottom: 38px;
        padding-top: 0px;
        text-align: left;
    }

    .LogonTextClassCSS .StaticText {
        color: #4a4a4a;
        font-size: 25px;
    }

    .LogonTextClassCSS .dxm-tmpl td.dxic > input, .WebEditorCell td.dxic > input {
        padding-bottom: 8px !important;
        padding-top: 8px !important;
    }


    .PasswordHintClassCSS {
        padding-bottom: 20px;
        text-align: left;
    }

    .PasswordHintClassCSS .StaticText { font-size: 12px; }

    @media all and (max-width: 480px), (max-height: 480px) {
        .LogonItemClassCSS {
            padding-bottom: 5px;
            padding-top: 5px;
        }

        .LogonContent { padding: 15px 25px; }
    }

    @media all and (max-width: 480px) {
        .LogonTextClassCSS { padding-bottom: 10px; }

        .LogonContentWidth { width: 100%; }

        .PasswordHintClassCSS { padding-bottom: 5px; }

        .LogonTextClassCSS .dxm-tmpl td.dxic > input, .WebEditorCell td.dxic > input {
            padding-bottom: 5px !important;
            padding-top: 5px !important;
        }
    }

    @media all and (max-height: 480px) {
        .LogonTextClassCSS { padding-bottom: 2px; }

        .LogonContent { padding: 15px 35px; }

        .PasswordHintClassCSS {
            padding-bottom: 2px;
            padding-top: 2px;
        }

        .LogonTextClassCSS .dxm-tmpl td.dxic > input, .WebEditorCell td.dxic > input {
            padding-bottom: 2px !important;
            padding-top: 2px !important;
        }

        .dxmLite_XafTheme .dxm-main.dxmtb { padding: 0px; }
    }

    @media all and (max-height: 350px) {
        .LogonTextClassCSS { padding-bottom: 2px; }

        .LogonContent { padding: 10px 25px; }

        .PasswordHintClassCSS {
            padding-bottom: 2px;
            padding-top: 2px;
        }

        .LogonTextClassCSS .dxm-tmpl td.dxic > input, .WebEditorCell td.dxic > input {
            padding-bottom: 2px !important;
            padding-top: 2px !important;
        }

        .dxmLite_XafTheme .dxm-main.dxmtb { padding: 0px; }
    }

    @media all and (max-height: 360px) {
        .LogonContent { padding: 10px 25px; }
    }

    @media all and (max-width: 450px) {
        .LogonContent { padding: 15px 35px; }

        .LogonMainTable.LogonContentWidth { width: 90%; }
    }

    @media all and (max-width: 400px) {
        .LogonMainTable.LogonContentWidth { width: 100%; }
    }
</style>
<xaf:XafUpdatePanel ID="UPPopupWindowControl" runat="server">
    <xaf:XafPopupWindowControl runat="server" id="PopupWindowControl"/>
</xaf:XafUpdatePanel>
<xaf:XafUpdatePanel ID="UPHeader" runat="server">
    <div class="white borderBottom width100" id="headerTableDiv">
        <div class="paddings sizeLimit" style="margin: auto">
            <table id="headerTable" class="headerTable xafAlignCenter white width100 sizeLimit" style="height: 60px;">
                <tbody>
                <tr>
                    <td>
                        <img src="Images/ProductImage.png"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</xaf:XafUpdatePanel>
<div style="position: absolute; top: 25%; width: 100%;">
    <table class="LogonMainTable LogonContentWidth">
        <% if (YouSoft.Vn.XAFCustom.Supports.CoreApp.Info.LicenseProblem)
           { %>
            <tr>
                <td align="right">
                    <img src="Images/ProductImage.png" height="200"/>
                </td>
                <td style="color: red">Bản quyền không hợp lệ. Vui lòng liên hệ <a href="http://yousoft.vn">YouSoft Việt Nam</a> để được hỗ trợ.</td>
            </tr>
        <% }
           else
           { %>
            <tr>
                <!--td rowspan="2" align="right" ><img src="Images/ProductImage.png" height="400" /--></!--td>
                <td>
                    <xaf:XafUpdatePanel ID="UPEI" runat="server" CssClass="LogonContentWidth">
                        <xaf:ErrorInfoControl ID="ErrorInfo" Style="margin: 10px 0px 10px 0px" runat="server"/>
                    </xaf:XafUpdatePanel>
                </td>
            </tr>
        <tr>
            <td>
                <table class="LogonContent LogonContentWidth">
                    <tr>
                        <td class="LogonContentCell">
                            <xaf:XafUpdatePanel ID="UPVSC" runat="server">
                                <xaf:ViewSiteControl ID="viewSiteControl" runat="server"/>
                            </xaf:XafUpdatePanel>

                            <xaf:XafUpdatePanel ID="UPPopupActions" runat="server" CssClass="right">
                                <xaf:ActionContainerHolder ID="PopupActions" runat="server" Orientation="Horizontal" ContainerStyle="Buttons">
                                    <Menu Width="100%" ItemAutoWidth="False"/>
                                    <ActionContainers>
                                        <xaf:WebActionContainer ContainerId="PopupActions"/>
                                    </ActionContainers>
                                </xaf:ActionContainerHolder>
                            </xaf:XafUpdatePanel>
                        </td>
                    </tr>

                    <% } %>
                </table>
            </td>
        </tr>
        </table>

</div>