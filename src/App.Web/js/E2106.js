﻿window.E2106 = window.E2106 || {};
window.E2106.HighlightFocusedLayoutItem = new function () {
    var NoColor = "";
	var nullText = "";
    this.onGotFocus = function (s, e) {
        var mainElement = s.GetMainElement();
        var targetElement = e.highlightParent === true ? findParentLayoutItem(mainElement) : mainElement;
        highlightElement(targetElement, e.backColor);
    };
	
    this.onLostFocus = function (s, e) {
        var mainElement = s.GetMainElement();
        var targetElement = e.highlightParent === true ? findParentLayoutItem(mainElement) : mainElement;
        highlightElement(targetElement, NoColor);
	
    };
	
	
    var highlightElement = function (element, backgroundColor) {
        if (element) {
            element.style.backgroundColor = backgroundColor;
        }
    }
    var findParentLayoutItem = function (element) {
        var allParents = document.querySelectorAll('div.Item');
        for (var i = 0, l = allParents.length; i < l; i++) {
            if (allParents[i].contains(element)) {
                return allParents[i];
            }
        }
        return null;
    };
};