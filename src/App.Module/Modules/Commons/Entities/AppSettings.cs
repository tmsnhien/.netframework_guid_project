﻿//-----------------------------------------------------------------------
// <copyright file="D:\01_Projects\sonhung\Implemenation\IMS\App.Module\Modules\M01_Common\Entities\Common\AppSettings.cs" company="">
//     Author:  
//     Copyright (c) . All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using YouSoft.Vn.XAFCustom.Core.Entities;

namespace ModuleBase.Entities
{
    [DefaultClassOptions]
    [XafDisplayName("Cấu hình hệ thống")]
    [ImageName("BO_AppSettings")]
    //[CreatableItem]
    [VisibleInDashboards]
    [VisibleInReports]
    [NavigationItem("ModuleBase.Entities")]
    [LookupEditorMode(LookupEditorMode.AllItemsWithSearch)]
    public partial class AppSettings : YsvAppSettings
    {
        public AppSettings(Session session)
                   : base(session)
        {
        }

        public new static AppSettings Instance = GetInstance<AppSettings>();
        
    }
}