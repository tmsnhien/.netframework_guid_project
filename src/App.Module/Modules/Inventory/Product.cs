﻿using DevExpress.ExpressApp.DC;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouSoft.Vn.XAFCustom.Core.Entities;
using YouSoft.Vn.XAFCustom.Supports.Attributes;
using YouSoft.Vn.XAFCustom.Supports.Attributes.Validation;

namespace App.Module.Modules.Inventory
{
    //gõ dn và dấu cách
    [XafDisplayName("Hàng hóa")]
    public class Product:YsvObject
    {
        //gõ xps để ra trường tên

        private double _price;
        private string _name;

        public Product(Session session) : base(session)
        {

        }

        public Product()
        {

        }

        [XafDisplayName("Tên")]
        //[ToolTip("")]
        [Size(SizeAttribute.DefaultStringMappingFieldSize)]
        public string Name
        {
            get => _name;
            set => SetPropertyValue(nameof(Name), ref _name, value);

        }
        
        //gõ xpd sẽ ra như bên dưới
        [XafDisplayName("Giá")]
        //[ToolTip("")]
        public double Price
        {
            get => _price;
            set => SetPropertyValue(nameof(Price), ref _price, value);
        }
    }
}
